# origami-world-test

Skeleton code for a 3D game in Unity. Contains rudimentary planet gravity and 3rd person camera.

![](Demos/gravity.gif)

![](Demos/demo.gif)